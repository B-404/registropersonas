<%-- 
    Document   : lista
    Created on : 01-05-2021, 17:39:43
    Author     : ELIAS
--%>
<%@page import="java.util.Iterator"%>
<%@page import="root.heroku.apirestregistropersonas.entity.BdRegistro"%>
<%@page import="java.util.List"%>
<%
    List<BdRegistro> solicitudes = (List<BdRegistro>) request.getAttribute("lista");
    Iterator<BdRegistro> itRegistro = solicitudes.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <form  name="form" action="BdRegistroController" method="POST">

       
        <table border="1">
                    <thead>
                    <th>Nombre</th>
                    <th>Rut </th>
                    <th>Email</th>
                    <th>Direccion </th>
             
                    <th> </th>
         
                    </thead>
                    <tbody>
                        <%while (itRegistro.hasNext()) {
                       BdRegistro cm = itRegistro.next();%>
                        <tr>
                            <td><%= cm.getNombre()%></td>
                            <td><%= cm.getRut()%></td>
                            <td><%= cm.getEmail()%></td>
                            <td><%= cm.getDireccion()%></td>
                 <td> <input type="radio" name="seleccion" value="<%= cm.getRut()%>"> </td>
                
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
            <button type="submit" name="accion" value="eliminar" class="btn btn-success">Eliminar Solicitud</button>
            <button type="submit" name="accion" value="Consultar" class="btn btn-success">Consultar</button>       
         </form>            
    </body>
</html>

