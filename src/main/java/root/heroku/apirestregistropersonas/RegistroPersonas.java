
package root.heroku.apirestregistropersonas;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.heroku.apirestregistropersonas.dao.BdRegistroJpaController;
import root.heroku.apirestregistropersonas.dao.exceptions.NonexistentEntityException;
import root.heroku.apirestregistropersonas.entity.BdRegistro;



@Path("registro")
public class RegistroPersonas {
    
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarRegistro(){
        
      BdRegistroJpaController dao=new BdRegistroJpaController();
      List<BdRegistro> lista=dao.findBdRegistroEntities();
      
       return Response.ok(200).entity(lista).build();
        
    }
    
    @GET
    @Path("/(idbuscar)")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultaPorRut(@PathParam("idbuscar")String idbusca){
    BdRegistroJpaController dao=new BdRegistroJpaController(); 
    BdRegistro bdregistro =dao.findBdRegistro(idbusca);
    
    
    return Response.ok(200).entity(bdregistro).build();
    } 
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(BdRegistro bdRegistro){
     
        try {
            BdRegistroJpaController dao=new BdRegistroJpaController();
            
            dao.create(bdRegistro);
        } catch (Exception ex) {
            Logger.getLogger(RegistroPersonas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(bdRegistro).build();
    }
    
    @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("iddelete")String iddelete){
     
        try {
            BdRegistroJpaController dao=new BdRegistroJpaController();
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(RegistroPersonas.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok("cliente eliminado").build();
    }
    
    @PUT
    public Response update(BdRegistro bdRegistro){
        
        try {
            BdRegistroJpaController dao=new BdRegistroJpaController();
            dao.edit(bdRegistro);
        } catch (Exception ex) {
            Logger.getLogger(RegistroPersonas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(bdRegistro).build();
    }
}
