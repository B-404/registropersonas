/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.heroku.apirestregistropersonas.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ELIAS
 */
@Entity
@Table(name = "bd_registro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BdRegistro.findAll", query = "SELECT b FROM BdRegistro b"),
    @NamedQuery(name = "BdRegistro.findByNombre", query = "SELECT b FROM BdRegistro b WHERE b.nombre = :nombre"),
    @NamedQuery(name = "BdRegistro.findByRut", query = "SELECT b FROM BdRegistro b WHERE b.rut = :rut"),
    @NamedQuery(name = "BdRegistro.findByEmail", query = "SELECT b FROM BdRegistro b WHERE b.email = :email"),
    @NamedQuery(name = "BdRegistro.findByDireccion", query = "SELECT b FROM BdRegistro b WHERE b.direccion = :direccion")})
public class BdRegistro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2147483647)
    @Column(name = "rut")
    private String rut;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 2147483647)
    @Column(name = "email")
    private String email;
    @Size(max = 2147483647)
    @Column(name = "direccion")
    private String direccion;

    public BdRegistro() {
    }

    public BdRegistro(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nombre != null ? nombre.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BdRegistro)) {
            return false;
        }
        BdRegistro other = (BdRegistro) object;
        if ((this.nombre == null && other.nombre != null) || (this.nombre != null && !this.nombre.equals(other.nombre))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.heroku.apirestregistropersonas.entity.BdRegistro[ nombre=" + nombre + " ]";
    }
    
}
