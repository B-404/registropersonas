/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.heroku.apirestregistropersonas.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.heroku.apirestregistropersonas.dao.exceptions.NonexistentEntityException;
import root.heroku.apirestregistropersonas.dao.exceptions.PreexistingEntityException;
import root.heroku.apirestregistropersonas.entity.BdRegistro;

/**
 *
 * @author ELIAS
 */
public class BdRegistroJpaController implements Serializable {

    public BdRegistroJpaController() {
        
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("clientes_PU");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(BdRegistro bdRegistro) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(bdRegistro);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findBdRegistro(bdRegistro.getNombre()) != null) {
                throw new PreexistingEntityException("BdRegistro " + bdRegistro + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(BdRegistro bdRegistro) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            bdRegistro = em.merge(bdRegistro);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = bdRegistro.getNombre();
                if (findBdRegistro(id) == null) {
                    throw new NonexistentEntityException("The bdRegistro with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            BdRegistro bdRegistro;
            try {
                bdRegistro = em.getReference(BdRegistro.class, id);
                bdRegistro.getNombre();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The bdRegistro with id " + id + " no longer exists.", enfe);
            }
            em.remove(bdRegistro);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<BdRegistro> findBdRegistroEntities() {
        return findBdRegistroEntities(true, -1, -1);
    }

    public List<BdRegistro> findBdRegistroEntities(int maxResults, int firstResult) {
        return findBdRegistroEntities(false, maxResults, firstResult);
    }

    private List<BdRegistro> findBdRegistroEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(BdRegistro.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public BdRegistro findBdRegistro(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(BdRegistro.class, id);
        } finally {
            em.close();
        }
    }

    public int getBdRegistroCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<BdRegistro> rt = cq.from(BdRegistro.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
