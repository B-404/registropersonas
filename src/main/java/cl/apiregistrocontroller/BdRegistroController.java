
package cl.apiregistrocontroller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import root.heroku.apirestregistropersonas.entity.BdRegistro;

/**
 *
 * @author ELIAS
 */
@WebServlet(name = "BdRegistroController", urlPatterns = {"/BdRegistroController"})
public class BdRegistroController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BdRegistroController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BdRegistroController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        String accion=request.getParameter("accion");
        
        if (accion.equals("Ingreso")){
            request.getRequestDispatcher("ingreso.jsp").forward(request, response);
            
            
            
        }
        
        
                if (accion.equals("registrosolicitud")){
                    
            BdRegistro bdRegistro=new BdRegistro();
            bdRegistro.setNombre(request.getParameter("nombre"));
            bdRegistro.setRut(request.getParameter("rut"));
            bdRegistro.setEmail(request.getParameter("email"));
            bdRegistro.setDireccion(request.getParameter("direccion"));        
            Client client = ClientBuilder.newClient();
            WebTarget myResource1=client.target("http://DESKTOP-TR5OGRJ:8080/apirestRegistroPersonas-1.0-SNAPSHOT/api/registro");
            BdRegistro bdRegistro1=myResource1.request(MediaType.APPLICATION_JSON).post(Entity.json(bdRegistro),BdRegistro.class);
       
               request.getRequestDispatcher("index.jsp").forward(request, response); 
        }
        
                
         if (accion.equals("verLista")){
             Client client = ClientBuilder.newClient();
            WebTarget myResource=client.target("http://DESKTOP-TR5OGRJ:8080/apirestRegistroPersonas-1.0-SNAPSHOT/api/registro");
             List<BdRegistro>lista = (List<BdRegistro>)myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<List<BdRegistro>>(){
            
             });
             request.setAttribute("lista", lista);
             request.getRequestDispatcher("lista.jsp").forward(request, response); 
         }        
        
        if (accion.equals("eliminar")){
            
            String idEliminar=request.getParameter("eliminar");
            Client client = ClientBuilder.newClient();
            WebTarget myResource1=client.target("http://DESKTOP-TR5OGRJ:8080/apirestRegistroPersonas-1.0-SNAPSHOT/api/registro/"+idEliminar);
            myResource1.request(MediaType.APPLICATION_JSON).delete();
            
            request.getRequestDispatcher("index.jsp").forward(request, response); 
        } 
         
        if (accion.equals("ver")){
            
            String idConsultar=request.getParameter("seleccion");
            Client client = ClientBuilder.newClient();
            WebTarget myResource1=client.target("http://DESKTOP-TR5OGRJ:8080/apirestRegistroPersonas-1.0-SNAPSHOT/api/registro/"+idConsultar);
            
            BdRegistro bdRegistro = myResource1.request(MediaType.APPLICATION_JSON).get(BdRegistro.class);
            request.setAttribute("bdRegistro", bdRegistro);
            request.getRequestDispatcher("editar.jsp").forward(request, response);
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
